import React, { Component } from 'react';
import './App.css';
import './Style.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div id="app"></div>
      </div>
    );
  }
}

export default App;
